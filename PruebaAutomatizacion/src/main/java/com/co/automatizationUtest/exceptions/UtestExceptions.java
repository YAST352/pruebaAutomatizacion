package com.co.automatizationUtest.exceptions;

import net.serenitybdd.core.exceptions.SerenityManagedException;

public class UtestExceptions extends SerenityManagedException {

    public static final String MESSAGE_FAILED_OPEN_BROWSER = "Fallo abriendo el navegador";
    public static final String MESSAGE_FAILED_REGISTER = "Fallo creando registro";

    public UtestExceptions(String message, Throwable testErrorException) {
        super(message, testErrorException);
        // TODO Auto-generated constructor stub
    }
}
