package com.co.automatizationUtest.ui;

import net.serenitybdd.screenplay.targets.Target;

public class RegisterDevices {

    public static final Target OPTIONCOMPUTER = Target.the("Select computer").locatedBy("//div[@id='web-device']/div/div[2]/div/div/span/i");
    public static final Target COMPUTER = Target.the("Computer").locatedBy("//input[@type='search']");
    public static final Target OPTIONVERSION = Target.the("Select version").locatedBy("//div[@id='web-device']/div[2]/div[2]/div/div/span/i");
    public static final Target VERSION = Target.the("Version").locatedBy("(//input[@type='search'])[2]");
    public static final Target OPTIONLANGUAGE = Target.the("Select language").locatedBy("//*[@id='web-device']/div[3]/div[2]/div/div[1]/span/span[2]");
    public static final Target LANGUAGE = Target.the("Language").locatedBy("(//input[@type='search'])[3]");
    public static final Target OPTIONMOBILE = Target.the("Select mobile").locatedBy("//div[@id='mobile-device']/div/div[2]/div/div/span/i");
    public static final Target MOBILE = Target.the("Mobile").locatedBy("(//input[@type='search'])[4]");

    public static final Target OPTIONMODEL = Target.the("Select model").locatedBy("//*[@id='mobile-device']/div[2]/div[2]/div/div[1]/span");
    public static final Target MODEL = Target.the("Model").locatedBy("(//input[@type='search'])[5]");
    public static final Target OPTIONOS = Target.the("Select OS").locatedBy("//div[@id='mobile-device']/div[3]/div[2]/div/div/span");
    public static final Target OS = Target.the("OS").locatedBy("(//input[@type='search'])[6]");
    public static final Target NEXTSECURITY = Target.the("Boton security").locatedBy("//section[@id='regs_container']/div/div[2]/div/div[2]/div/div[2]/div/a/span");;
}
