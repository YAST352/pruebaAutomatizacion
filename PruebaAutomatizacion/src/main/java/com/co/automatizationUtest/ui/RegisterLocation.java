package com.co.automatizationUtest.ui;

import net.serenitybdd.screenplay.targets.Target;

public class RegisterLocation {
    public static final Target CITY = Target.the("City").locatedBy("//input[@id='city']");
    public static final Target CODEPOSTAL = Target.the("Code postal - zip").locatedBy("//input[@id='zip']");
    public static final Target NEXTDEVICES = Target.the("Boton devices").locatedBy("//section[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[2]/div/a/span");

}
