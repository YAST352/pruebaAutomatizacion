package com.co.automatizationUtest.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.utest.com/")
public class HomePageUtest extends PageObject {
    public static final Target OPTIONREGISTER = Target.the("Register in uTest").locatedBy("//a[contains(text(),'Join Today')]");
}
