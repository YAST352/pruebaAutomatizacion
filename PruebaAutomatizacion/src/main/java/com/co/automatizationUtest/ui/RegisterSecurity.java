package com.co.automatizationUtest.ui;

import net.serenitybdd.screenplay.targets.Target;

public class RegisterSecurity {

    public static final Target PASSWORD = Target.the("PASSWORD").locatedBy("//input[@id='password']");
    public static final Target CONFIRMPASSWORD = Target.the("Confirm PASSWORD").locatedBy("//input[@id='confirmPassword']");
    public static final Target OPTIONALINFORMED = Target.the("Optional information").locatedBy("//section[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[4]/label/span");
    public static final Target CODECONDUCT = Target.the("Code of conduct").locatedBy("//*[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]");
    public static final Target PRIVACYSECURITY = Target.the("Privacity and security").locatedBy("//*[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]");
    public static final Target COMPLETE = Target.the("Boton finish").locatedBy("//a[@id='laddaBtn']/span");
}
