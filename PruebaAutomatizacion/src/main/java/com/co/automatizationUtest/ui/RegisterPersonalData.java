package com.co.automatizationUtest.ui;

import net.serenitybdd.screenplay.targets.Target;

public class RegisterPersonalData {

    public static final Target NAME = Target.the("First name").locatedBy("//input[@id='firstName']");
    public static final Target LASTNAME = Target.the("Last name").locatedBy("//input[@id='lastName']");
    public static final Target EMAIL = Target.the("E-mail").locatedBy("//input[@id='email']");
    public static final Target MONTH = Target.the("Birth month").locatedBy("//select[@id='birthMonth']");
    public static final Target DAY = Target.the("Birth day").locatedBy("//select[@id='birthDay']");
    public static final Target YEAR = Target.the("Birth year").locatedBy("//select[@id='birthYear']");
    public static final Target NEXTLOCATION = Target.the("Boton next location").locatedBy("//section[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[2]/a/span");
}
