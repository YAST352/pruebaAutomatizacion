package com.co.automatizationUtest.other;

import java.awt.*;
import java.awt.event.InputEvent;

public class MoveRobot {
    //instanciamos la clase Robot
    Robot robot = new Robot();

    public MoveRobot() throws AWTException {
        //cambia la posición en pantalla del puntero a las coordenadas
        //X=300 e Y=600.
        robot.mouseMove(600, 520);
        //dormimos el robot por 250 mili segundos luego de usar cada tecla
        robot.delay(250);
        //click con el boton izquierdo
        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }
}
