package com.co.automatizationUtest.model;

public class RegisterModel {
    private String name;
    private String lastName;
    private String email;
    private String month;
    private String day;
    private String year;
    private String city;
    private String postal;
    private String computer;
    private String version;
    private String language;
    private String mobile;
    private String model;
    private String os;
    private String password;
    private String confirmPassword;

    public RegisterModel(String name, String lastName, String email, String month, String day, String year, String city, String postal) {

        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.month = month;
        this.day = day;
        this.year = year;
        this.city = city;
        this.postal = postal;

    }

    public RegisterModel(String computer, String version, String language, String mobile, String model, String os){

        this.computer = computer;
        this.version = version;
        this.language = language;
        this.mobile = mobile;
        this.model = model;
        this.os = os;

    }

    public RegisterModel(String password, String confirmPassword) {

        this.password = password;
        this.confirmPassword = confirmPassword;

    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getMonth() {
        return month;
    }

    public String getDay() {
        return day;
    }

    public String getYear() {
        return year;
    }

    public String getCity() {
        return city;
    }

    public String getPostal() {
        return postal;
    }

    public String getComputer() {
        return computer;
    }

    public String getVersion() {
        return version;
    }

    public String getLanguage() {
        return language;
    }

    public String getMobile() {
        return mobile;
    }

    public String getModel() {
        return model;
    }

    public String getOs() {
        return os;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }
}
