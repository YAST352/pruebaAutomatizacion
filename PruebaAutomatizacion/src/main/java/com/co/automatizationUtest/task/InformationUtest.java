package com.co.automatizationUtest.task;

import com.co.automatizationUtest.exceptions.UtestExceptions;
import com.co.automatizationUtest.model.RegisterModel;
import com.co.automatizationUtest.ui.RegisterDevices;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class InformationUtest implements Task {
    RegisterModel data;

    public InformationUtest(RegisterModel data){
        this.data =data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        RegisterInformation(actor);
    }

    private <T extends Actor> void RegisterInformation(T actor) {
        try {
            actor.wasAbleTo(
                    Click.on(RegisterDevices.OPTIONCOMPUTER),
                    Enter.theValue(data.getComputer()).into(RegisterDevices.COMPUTER),
                    Click.on(RegisterDevices.OPTIONVERSION),
                    Enter.theValue(data.getVersion()).into(RegisterDevices.VERSION),
                    Click.on(RegisterDevices.OPTIONLANGUAGE),
                    Enter.theValue(data.getLanguage()).into(RegisterDevices.LANGUAGE),
                    Click.on(RegisterDevices.OPTIONMOBILE),
                    Enter.theValue(data.getMobile()).into(RegisterDevices.MOBILE).thenHit(Keys.TAB),
                    Click.on(RegisterDevices.OPTIONMODEL),
                    Enter.theValue(data.getModel()).into(RegisterDevices.MODEL).thenHit(Keys.TAB),
                    Click.on(RegisterDevices.OPTIONOS),
                    Enter.theValue(data.getOs()).into(RegisterDevices.OS),
                    Click.on(RegisterDevices.NEXTSECURITY)
            );

        } catch (Exception e) {
            throw new UtestExceptions(UtestExceptions.MESSAGE_FAILED_REGISTER, e);
        }
    }


    public static InformationUtest add(RegisterModel data) {
        return instrumented(InformationUtest.class, data);
    }
}
