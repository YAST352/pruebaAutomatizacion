package com.co.automatizationUtest.task;

import com.co.automatizationUtest.exceptions.UtestExceptions;
import com.co.automatizationUtest.model.RegisterModel;
import com.co.automatizationUtest.ui.RegisterSecurity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SecurityUtest implements Task {
    RegisterModel data;

    public SecurityUtest(RegisterModel data){
        this.data =data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        RegisterSecurity(actor);
    }

    private <T extends Actor> void RegisterSecurity(T actor) {
        try {
            actor.attemptsTo(
                    Enter.theValue(data.getPassword()).into(RegisterSecurity.PASSWORD),
                    Enter.theValue(data.getConfirmPassword()).into(RegisterSecurity.CONFIRMPASSWORD),
                    Click.on(RegisterSecurity.OPTIONALINFORMED),
                    Click.on(RegisterSecurity.CODECONDUCT),
                    Click.on(RegisterSecurity.PRIVACYSECURITY),
                    Click.on(RegisterSecurity.COMPLETE)
            );

        } catch (Exception e) {
            throw new UtestExceptions(UtestExceptions.MESSAGE_FAILED_REGISTER, e);
        }
    }

    public static SecurityUtest profile(RegisterModel data) {
        return instrumented(SecurityUtest.class, data);
    }
}
