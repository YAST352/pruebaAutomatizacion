package com.co.automatizationUtest.task;

import com.co.automatizationUtest.exceptions.UtestExceptions;
import com.co.automatizationUtest.ui.HomePageUtest;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class HomeUtest implements Task {
    private PageObject page2;
    HomePageUtest page;

    public HomeUtest(PageObject page){
        this.page2 =page;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        register(actor);
    }

    private <T extends Actor> void register(T actor) {
        try {
            actor.attemptsTo(Open.browserOn(page));
        } catch (Exception e) {
            throw new UtestExceptions(UtestExceptions.MESSAGE_FAILED_OPEN_BROWSER, e);
        }
    }

    public static HomeUtest at (PageObject page) {
        return instrumented(HomeUtest.class, page);
    }

}
