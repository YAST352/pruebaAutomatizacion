package com.co.automatizationUtest.task;

import com.co.automatizationUtest.exceptions.UtestExceptions;
import com.co.automatizationUtest.model.RegisterModel;
import com.co.automatizationUtest.other.MoveRobot;
import com.co.automatizationUtest.ui.HomePageUtest;
import com.co.automatizationUtest.ui.RegisterLocation;
import com.co.automatizationUtest.ui.RegisterPersonalData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegisterUtest implements Task {

    RegisterModel data;

    public RegisterUtest(RegisterModel data){
        this.data =data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        personalData(actor);

    }

    private <T extends Actor> void personalData(T actor) {
        try {
            actor.wasAbleTo(
                    Click.on(HomePageUtest.OPTIONREGISTER),
                    Enter.theValue(data.getName()).into(RegisterPersonalData.NAME),
                    Enter.theValue(data.getLastName()).into(RegisterPersonalData.LASTNAME),
                    Enter.theValue(data.getEmail()).into(RegisterPersonalData.EMAIL),
                    SelectFromOptions.byVisibleText(data.getMonth()).from(RegisterPersonalData.MONTH),
                    SelectFromOptions.byVisibleText(data.getDay()).from(RegisterPersonalData.DAY),
                    SelectFromOptions.byVisibleText(data.getYear()).from(RegisterPersonalData.YEAR),
                    Click.on(RegisterPersonalData.NEXTLOCATION),
                    Enter.theValue(data.getCity()).into(RegisterLocation.CITY)
            );
            new MoveRobot();
            actor.attemptsTo(
                    Enter.theValue(data.getPostal()).into(RegisterLocation.CODEPOSTAL),
                    Click.on(RegisterLocation.NEXTDEVICES)
            );

        } catch (Exception e) {
            throw new UtestExceptions(UtestExceptions.MESSAGE_FAILED_REGISTER, e);
        }

    }

    public static RegisterUtest at(RegisterModel data) {
        return instrumented(RegisterUtest.class, data);
    }
}
