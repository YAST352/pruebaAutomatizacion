package com.co.automatizationUtest.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src/test/resources/features", 
		glue= "com.co.automatizationUtest.stepDefinitions",
		snippets= SnippetType.CAMELCASE
		)

public class UtestRunner {}
