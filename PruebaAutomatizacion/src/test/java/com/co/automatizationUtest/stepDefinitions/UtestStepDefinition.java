package com.co.automatizationUtest.stepDefinitions;

import com.co.automatizationUtest.model.RegisterModel;
import com.co.automatizationUtest.task.HomeUtest;
import com.co.automatizationUtest.task.InformationUtest;
import com.co.automatizationUtest.task.RegisterUtest;
import com.co.automatizationUtest.task.SecurityUtest;
import com.co.automatizationUtest.ui.HomePageUtest;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

public class UtestStepDefinition {

    @Managed() private WebDriver driver;
    private Actor yazmin = Actor.named("Yazmin");
    private HomePageUtest uTest;

    @Before
    public void setup() {
        yazmin.can(BrowseTheWeb.with(driver));
    }

    @Given("^I want to create user in uTest with personal data (.*), (.*), (.*), (.*), (.*), (.*), (.*), (.*)$")
    public void iWantToCreateUserInUTestWithPersonalData(String name, String lastName, String email, String month, String day, String year, String city, String postal) {
        yazmin.wasAbleTo(HomeUtest.at(uTest));
        yazmin.attemptsTo(RegisterUtest.at(new RegisterModel(name, lastName, email, month, day, year, city, postal)));
    }

    @When("^I add device registration information (.*), (.*), (.*), (.*), (.*), (.*) and setup process (.*), (.*)$")
    public void iAddDeviceRegistrationInformationAndSetupProcess(String computer, String version, String language, String mobile, String model, String os, String password, String confirmPassword) {
        yazmin.attemptsTo(InformationUtest.add(new RegisterModel(computer, version, language, mobile, model, os)));
        yazmin.attemptsTo(SecurityUtest.profile(new RegisterModel(password, confirmPassword)));

    }

    @Then("^I should see successful registration$")
    public void iShouldSeeSuccessfulRegistration() {

    }


}
