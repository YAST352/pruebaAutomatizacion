Feature: Create user record
  I as user
  I want to create a record of a user profile in uTest.

  @tag
  Scenario Outline: create user on uTest website
    Given I want to create user in uTest with personal data <name>, <lastName>, <email>, <month>, <day>, <year>, <city>, <postal>
    When I add device registration information <computer>, <version>, <language>, <mobile>, <model>, <os> and setup process <password>, <confirmPassword>
    Then I should see successful registration

    Examples:
    | name | lastName |       email        |  month  | day | year |  city  | postal | computer | version | language | mobile |   model    |    os    | password | confirmPassword |
    |Yazmin|Sucerquia |stjazmin@hotmail.com|September|1    |1993  |Medellin|050014  |Windows   |10       |Spanish   |Xiaomi  |Redmi Note 8|Android 10|Abcdefgh12|Abcdefgh12       |

